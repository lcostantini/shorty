# frozen_string_literal: true

require 'cuba'
require 'json'

Cuba.settings[:default_headers] = { 'Content-Type' => 'application/json' }

# rubocop:disable Metrics/BlockLength
Cuba.define do
  on post do
    on 'shorten' do
      on param('url'), param('shortcode') do |_url, shortcode|
        if /^[0-9a-zA-Z_]{4,}$/.match?(shortcode)
          res.status = 201
          res.write({ shortcode: shortcode }.to_json)
        else
          error_message = "Shorcode: '#{shortcode}' fails to " \
            'meet the regexp: ^[0-9a-zA-Z_]{4,}$'

          res.status = 409
          res.write({ error: error_message }.to_json)
        end
      end

      on param('url') do |_url|
        res.status = 201
        res.write({ shortcode: 'fooBAR789' }.to_json)
      end

      on true do
        res.status = 404
        res.write({ error: 'URL needs to be present' }.to_json)
      end
    end
  end

  on get do
    on ':shortcode' do |_shortcode|
      on root do
        res.status = 302
        res.[]=('Location', 'http://example.com')
      end

      on 'stats' do
        response = {
          startDate: '2012-04-23T18:25:43.511Z',
          lastSeenDate: '2012-04-23T18:25:43.511Z',
          redirectCount: 1
        }

        res.status = 200
        res.write(response.to_json)
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
