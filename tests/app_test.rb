# frozen_string_literal: true

require 'cuba/test'
require './app'

def body
  JSON.parse(last_response.body, symbolize_names: true)
end

setup do
  {
    url: 'http://example.com',
    shortcode: 'fooBAR789'
  }
end

# rubocop:disable Metrics/BlockLength
scope '#post /shorten' do
  scope 'success response' do
    test 'when only the URL is sent' do |params|
      post '/shorten', url: params[:url]

      assert body[:shortcode]
      assert_equal 201, last_response.status
      assert_equal 'application/json', last_response.header['Content-Type']
    end

    test 'when the URL and shortcode are sent' do |params|
      post '/shorten', url: params[:url], shortcode: params[:shortcode]

      assert_equal params[:shortcode], body[:shortcode]
      assert_equal 201, last_response.status
      assert_equal 'application/json', last_response.header['Content-Type']
    end
  end

  scope 'failure response' do
    test 'return 404 when the URL is not present' do
      post '/shorten'

      assert_equal 'URL needs to be present', body[:error]
      assert_equal 404, last_response.status
      assert_equal 'application/json', last_response.header['Content-Type']
    end

    # test 'return 409 when the shortcode is in use' do |params|
    #   post '/shorten', shortcode: params[:shortcode]

    #   error_msg = "The shorcode: '#{params[:shorcode]}' is already in use"

    #   assert_equal error_msg, body[:error]
    #   assert_equal 409, last_response.status
    #   assert_equal 'application/json', last_response.header['Content-Type']
    # end

    test 'return 422 when the shortcode is invalid' do |params|
      post '/shorten', url: params[:url], shortcode: 'foo'

      error_msg = "Shorcode: 'foo' fails to meet the regexp: ^[0-9a-zA-Z_]{4,}$"

      assert_equal error_msg, body[:error]
      assert_equal 409, last_response.status
      assert_equal 'application/json', last_response.header['Content-Type']
    end
  end
end

scope '#get' do
  scope '/:shortcode' do
    scope 'success response' do
      test 'with valid shortcode return location header with URL' do |params|
        get "/#{params[:shortcode]}"

        assert_equal 302, last_response.status
        assert_equal params[:url], last_response.header['Location']
        assert_equal 'application/json', last_response.header['Content-Type']
      end
    end

    # scope 'failure response' do
    #   test 'when the shortcode is missing' do
    #     get '/missingCode'

    #     error_msg = "The shortcode: 'missingCode' can not be " \
    #       'found in the system'

    #     assert_equal error_msg, body[:error]
    #     assert_equal 404, last_response.status
    #     assert_equal 'application/json', last_response.header['Content-Type']
    #   end
    # end
  end

  scope '/stats' do
    scope 'success response' do
      test 'when the shortcode is correct' do |params|
        get "/#{params[:shortcode]}/stats"

        response = {
          startDate: '2012-04-23T18:25:43.511Z',
          lastSeenDate: '2012-04-23T18:25:43.511Z',
          redirectCount: 1
        }

        assert_equal response, body
        assert_equal 200, last_response.status
        assert_equal 'application/json', last_response.header['Content-Type']
      end
    end

    # scope 'failure response' do
    #   test 'when the shortcode is missing' do
    #     get '/missingCode/stats'

    #     error_msg = "The shortcode: 'missingCode' can " \
    #       'not be found in the system'

    #     assert_equal error_msg, body[:error]
    #     assert_equal 404, last_response.status
    #     assert_equal 'application/json', last_response.header['Content-Type']
    #   end
    # end
  end
end
# rubocop:enable Metrics/BlockLength
